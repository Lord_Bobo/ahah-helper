(function ($) {
  if (!$.fn.detach) {
    $.fn.detach = function () {
      return this.each(function () {
        if (this.parentNode) {
          this.parentNode.removeChild(this);
        }
      });
    };
  }
}(jQuery));

(function($) {

if (Drupal.jsEnabled) {
  $(document).ready(function() {
    if (Drupal.ahah != undefined) {

/**
 * Override of Drupal.ahah.prototype.success. The only difference is that we
 * allow for new Drupal.settings.
 */
Drupal.ahah.prototype.success = function (response, status) {
  var wrapper = $(this.wrapper);
  var form = $(this.element).parents('form');
  // Safari without Webkit mentioned in ticket http://dev.jquery.com/ticket/1152 is now obsolete, so we can parse the response the normal way
  var new_content = $(response.data);

  // Restore the previous action and target to the form.
  form.attr('action', this.form_action);
  this.form_target ? form.attr('target', this.form_target) : form.removeAttr('target');
  this.form_encattr ? form.attr('target', this.form_encattr) : form.removeAttr('encattr');

  // Remove the progress element.
  if (this.progress.element) {
    $(this.progress.element).remove();
  }
  if (this.progress.object) {
    this.progress.object.stopMonitoring();
  }
  $(this.element).removeClass('progress-disabled').attr('disabled', false);

  // Update status messages DIV
  updateMessages(response.messages);

  if (response.success) {
    // Trigger AHAH form succession
    var preventDefault = form.triggerHandler('ahah_success', [response]);
    // Only load the content if the trigger handler didn't return false
    if (preventDefault !== false) {
      // Add the new content to the page.
      Drupal.freezeHeight();
      if (this.method == 'replace' || this.method == 'replace_popup') {
        // Remove old content wrapper, if it has the same #id as the wrapper in the DOM
        var dupl_wrapper = new_content.find('*').andSelf().filter('#' + wrapper.attr('id'));
        if (dupl_wrapper.length > 0) {
          wrapper.replaceWith(new_content);
        } else {
          wrapper.empty().append(new_content);
        }
        if (this.method == 'replace_popup') {
          // Show obtained form copy in a dialog window
          var tmp_form = form.clone(true);
          var hiddens = tmp_form.find('input[name=form_build_id], input[name=form_token], input[name=form_id]').detach();
          tmp_form.empty();
          tmp_form.append(hiddens);
          // Add an invisible placeholder to the place from which the element will be moved
          var placeholder = $("<div class='ahah-dialog-placeholder'></div>");
          new_content.before(placeholder);
          placeholder.hide();
          tmp_form.append(new_content);

          var dialog = tmp_form.dialog({
            title: null,
            modal: true,
            closeOnEscape: true,
            width: 450,
            minHeight: 0,
            open: function (event, ui) {
              var $dialog = $(this);
              $('.ui-widget-overlay').click(function () {
                $dialog.dialog('close');
              });
            },
            close: function (event, ui) {
              // Move the element to its original location
              placeholder.after(new_content);
              placeholder.remove();
              $(this).dialog('destroy');
              $(this).remove();
            }
          });
          // Close the dialog upon form submit
          tmp_form.bind('ahah_success', function () {
            dialog.dialog('destroy');
            dialog.remove();
          });
        }
      }
      else {
        wrapper[this.method](new_content);
      }

      // Immediately hide the new content if we're using any effects.
      if (this.showEffect != 'show') {
        new_content.hide();
      }

      // Determine what effect use and what content will receive the effect, then
      // show the new content. For browser compatibility, Safari is excluded from
      // using effects on table rows.
      if (($.browser.safari && $("tr.ahah-new-content", new_content).size() > 0)) {
        new_content.show();
      }
      else if ($('.ahah-new-content', new_content).size() > 0) {
        $('.ahah-new-content', new_content).hide();
        new_content.show();
        $(".ahah-new-content", new_content)[this.showEffect](this.showSpeed);
      }
      else if (this.showEffect != 'show') {
        new_content[this.showEffect](this.showSpeed);
      }

      // Merge in new and changed settings, if any.
      if (response.settings) {
        $.extend(Drupal.settings, response.settings);
      }

      // Attach all javascript behaviors to the new content, if it was successfully
      // added to the page, this if statement allows #ahah[wrapper] to be optional.
      if (new_content.parents('html').length > 0) {
        Drupal.attachBehaviors(new_content);
      }
    }

    Drupal.unfreezeHeight();
  } else {
    // Just append errors to the fields that caused them (and clear all the previous errors)
    $('.form-item .error', form).remove();
    for (var id in response.errors) {
      // TODO: Solve also date pop-up field here
      var $appendTo = null;
      var $el = Array();
      if (($el = $('label[for=edit-' + idToId(id) + ']', form)).length > 0) {
        $appendTo = $el;
      } else if (($el = $('input[name="' + idToName(id) + '"]', form)).length > 0) {
        $appendTo = $el.closest('.form-radios').siblings('label');
      } else {
        $appendTo = new Array();
      }
      if ($appendTo.length > 0) {
        $appendTo.append("<span class='error'>" + response.errors[id] + "</span>");
      }
    }
  }
  if (!response.success || response.show_dialog) {
    // Show messages in a dialog window
    $(response.messages).dialog({
      title: Drupal.t("Warning"),
      modal: true,
      closeOnEscape: true,
      width: 450,
      minHeight: 0,
      open: function (event, ui) {
        $dialog = $(this);
        $('.ui-widget-overlay').click(function () {
          $dialog.dialog('close');
        });
      },
      close: function (event, ui) {
        $(this).dialog('destroy');
        $(this).remove();
      }
    });
  }
};

function updateMessages(html) {
  // TODO: Retrieve specific ID of the message container from some configuration settings
  $('#message-container').html(html);
}

// Convert from lorem][ipsum][dolor to lorem-ipsum-dolor
function idToId(id) {
  return id.replace(/\]\[/g, '-');
}

// Convert from lorem][ipsum][dolor to lorem[ipsum][dolor]
function idToName(id) {
  if (id.indexOf(']') == -1) {
    return id;
  } else {
    return id.replace(/\]/, '') + ']';
  }
}

    }
  });
}

})(jQuery);
